module gitlab.com/akita/vis

go 1.20

require (
	github.com/go-sql-driver/mysql v1.7.1
	github.com/mattn/go-sqlite3 v1.14.16
	gitlab.com/akita/akita/v3 v3.0.0-alpha.23
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/rs/xid v1.5.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/tebeka/atexit v0.3.0 // indirect
)

replace gitlab.com/akita/akita/v3 => ../akita
